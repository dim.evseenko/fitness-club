const swiper = new Swiper('.swiper', {
  slidesPerView: 5,
  spaceBetween: 0,
  // Optional parameters
  loop: true,
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    // when window width is >= 320px
    950: {
      slidesPerView: 3,
    },
    // when window width is >= 480px
    1080: {
      slidesPerView: 4,
    },
    // when window width is >= 640px
    1400: {
      slidesPerView: 5,
    }
  }
});